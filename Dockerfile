FROM golang:1.18-alpine3.15 AS app_builder
WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /build/service /build/cmd/main.go


FROM alpine:3.15
COPY --from=app_builder /build/service /
ENTRYPOINT ["/service"]
