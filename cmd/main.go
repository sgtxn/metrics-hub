package main

import (
	"log"
	"metrics-hub/internal/controller"
	"metrics-hub/internal/router"
	"metrics-hub/internal/service"
	"net/http"
	"time"
)

// TODO: move these to a proper env-based config
const (
	serverPort = ":8080"
	timeout    = time.Second * 5
)

// TODO: use a proper logging library
func main() {
	svc := service.New()
	cont := controller.New(svc)
	r := router.New(cont)

	srv := http.Server{
		Addr:         serverPort,
		Handler:      r,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}

	log.Println("Started listening at port " + serverPort)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatalln(err)
	}
}
