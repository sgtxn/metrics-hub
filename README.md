# Metrics Hub

## Description
This is a simple service for tracking the duration of some arbitrary long-running process and finding the outliers across different servers.

## Caution
Since this application is more of a prototype, it's definitely not quite in the production-ready state as it is.

First, this application uses the simplest possible type of persistence, which is in-memory cache. Surely, this solution should be quite fast, but this is not a scalable approach at all, since data is going to be spread across servers with unpredictable results. Some inter-service communication could fix this, but then it sounds like re-implementing distributed cache from scratch, so possibly the better option would be switching to something like Redis.

Also the application doesn't have any configurable options, validation, and the entire storage layer is a part of the service rather than being isolated and dependency-injected into it properly, which is okay for a simple implementation like this, but complicates the switch to a proper storage provider.

Test coverage is pretty limited for now, so increasing it would be quite an important thing to do as well.

## Execution

### Native
* Make sure you have the latest version of golang installed on your system
* Execute `go run cmd/main.go` from the root of the repository folder
* The application will be available at port `8080`
* For persistent builds you might be interested in `go build` or `go install` commands

### Docker
#### The simple way:
* Make sure you have the latest version of docker available on your system
* `docker run -p 8080:8080 registry.gitlab.com/sgtxn/metrics-hub:1.0.0`
* The application will be available at port `8080`

#### The build it yourself way:
* Make sure you have the latest version of docker available on your system
* cd to the root of the repository folder
* `docker build -t metrics-hub .`
* `docker run -p 8080:8080 metrics-hub`
* The application will be available at port `8080`
