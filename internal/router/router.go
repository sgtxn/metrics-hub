package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type controller interface {
	ProcessReport(http.ResponseWriter, *http.Request)
	ProcessStatistics(http.ResponseWriter, *http.Request)
	ProcessOutliers(http.ResponseWriter, *http.Request)
}

func New(cont controller) *chi.Mux {
	mux := chi.NewMux()

	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	mux.Post("/process_report", cont.ProcessReport)
	mux.Get("/process_statistics", cont.ProcessStatistics)
	mux.Get("/process_outliers", cont.ProcessOutliers)

	return mux
}
