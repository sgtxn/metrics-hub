package service

import (
	"errors"
	"math"
	"metrics-hub/internal/models"
	"sync"
	"time"
)

type durationCache struct {
	longestRun  int
	shortestRun int
}

type service struct {
	sync.RWMutex
	processDurationCache map[string]durationCache
	mean                 float64
	quadraticVariance    float64 // used for calculating running standart deviation
	totalReports         int
}

func New() *service {
	return &service{
		processDurationCache: make(map[string]durationCache),
	}
}

func (svc *service) ProcessReport(serverName string, startTime, endTime time.Time) error {
	svc.Lock()
	defer svc.Unlock()

	duration := findDurationInSeconds(startTime, endTime)

	// update the longest and shortest durations cache
	cache, ok := svc.processDurationCache[serverName]
	if ok {
		if duration > cache.longestRun {
			cache.longestRun = duration
		} else if duration < cache.shortestRun {
			cache.shortestRun = duration
		}
	} else {
		cache = durationCache{longestRun: duration, shortestRun: duration}
	}
	svc.processDurationCache[serverName] = cache

	// update total mean and quadratic variance
	if svc.totalReports == 0 {
		svc.mean = float64(duration)
		svc.quadraticVariance = 0
		svc.totalReports++
		return nil
	}

	newMean := countNewMeanTime(svc.mean, duration, svc.totalReports)
	newVariance := countNewVariance(svc.quadraticVariance, newMean, svc.mean, duration)

	svc.mean = newMean
	svc.quadraticVariance = newVariance
	svc.totalReports++

	return nil
}

func (svc *service) ProcessStatistics() (*models.Statistics, error) {
	svc.RLock()
	defer svc.RUnlock()

	// TODO: move magic numbers to config
	if svc.totalReports < 10 {
		return nil, errors.New("not enough reports received to generate statistics")
	}

	deviation := countStdDeviationFromVariance(svc.quadraticVariance, svc.totalReports)

	return &models.Statistics{MeanTime: int(math.Round(svc.mean)), StdDeviation: deviation}, nil
}

func (svc *service) ProcessOutliers() ([]string, error) {
	svc.RLock()
	defer svc.RUnlock()

	if svc.totalReports < 10 {
		return nil, errors.New("not enough reports received to find outliers")
	}

	result := make([]string, 0, len(svc.processDurationCache))

	deviation := countStdDeviationFromVariance(svc.quadraticVariance, svc.totalReports)
	targetDeviation := float64(deviation * 3)

	minValue := svc.mean - targetDeviation
	maxValue := svc.mean + targetDeviation

	for serverName, durations := range svc.processDurationCache {
		if float64(durations.longestRun) > maxValue || float64(durations.shortestRun) < minValue {
			result = append(result, serverName)
		}
	}

	return result, nil
}
