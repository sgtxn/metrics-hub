package service

import (
	"math"
	"time"
)

func findDurationInSeconds(startTime, endTime time.Time) int {
	return int(endTime.Sub(startTime).Seconds())
}

func countNewMeanTime(oldMeanTime float64, diff, numReports int) float64 {
	return (oldMeanTime*float64(numReports) + float64(diff)) / float64(numReports+1)
}

func countNewVariance(oldVariance, newMeanTime, oldMeanTime float64, diff int) float64 {
	return oldVariance + (float64(diff)-oldMeanTime)*(float64(diff)-newMeanTime)
}

func countStdDeviationFromVariance(variance float64, totalReports int) int {
	if totalReports < 1 {
		return 0
	}
	return int(math.Round(math.Sqrt(variance / float64(totalReports))))
}
