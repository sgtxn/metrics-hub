package service

import (
	"testing"
	"time"
)

type testCase struct {
	inputs           []testInput
	expectedMean     float64
	expectedVariance float64
}

type testInput struct {
	serverName string
	startTime  time.Time
	endTime    time.Time
}

func Test_ProcessReport_Succeeds(t *testing.T) {
	svc := New()

	now := time.Now()
	test := testCase{
		inputs: []testInput{
			{serverName: "test-server", startTime: now, endTime: now.Add(time.Second * 2)},
			{serverName: "test-server", startTime: now, endTime: now.Add(time.Second * 4)},
			{serverName: "test-server", startTime: now, endTime: now.Add(time.Second * 8)},
			{serverName: "test-server", startTime: now, endTime: now.Add(time.Second * 16)},
			{serverName: "test-server", startTime: now, endTime: now.Add(time.Second * -100)},
		},
		expectedMean:     -14,
		expectedVariance: 9360,
	}

	for _, testCase := range test.inputs {
		err := svc.ProcessReport(testCase.serverName, testCase.startTime, testCase.endTime)
		if err != nil {
			t.Fatal(err)
		}
	}

	if svc.mean != test.expectedMean ||
		svc.quadraticVariance != test.expectedVariance ||
		svc.totalReports != len(test.inputs) {
		t.Fatalf("expected a different mean/variance/reports, but got mean: %f, variance: %f, reports: %d",
			svc.mean, svc.quadraticVariance, svc.totalReports)
	}
}

func Test_ProcessStatistics_Succeeds(t *testing.T) {
	svc := New()

	svc.mean = 5.5
	svc.quadraticVariance = 82.5
	svc.totalReports = 10

	stats, err := svc.ProcessStatistics()
	if err != nil {
		t.Fatal(err)
	}

	if stats.MeanTime != 6 || stats.StdDeviation != 3 {
		t.Fatalf("expected a different mean/variance, but got mean: %f, variance: %f", svc.mean, svc.quadraticVariance)
	}
}
