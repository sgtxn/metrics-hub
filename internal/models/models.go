package models

type Statistics struct {
	MeanTime     int `json:"mean"`
	StdDeviation int `json:"stddev"`
}
