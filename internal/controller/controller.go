package controller

import (
	"encoding/json"
	"log"
	"metrics-hub/internal/models"
	"net/http"
	"time"
)

type servicer interface {
	ProcessReport(serverName string, startTime, endTime time.Time) error
	ProcessStatistics() (*models.Statistics, error)
	ProcessOutliers() ([]string, error)
}

type controller struct {
	svc servicer
}

func New(svc servicer) *controller {
	return &controller{svc: svc}
}

func (c *controller) ProcessReport(w http.ResponseWriter, r *http.Request) {
	body := struct {
		ServerName string    `json:"server_name"`
		StartTime  time.Time `json:"start_time"`
		EndTime    time.Time `json:"end_time"`
	}{}

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	if err := dec.Decode(&body); err != nil {
		log.Println(err)
		sendError(err, w, http.StatusBadRequest)

		return
	}

	// TODO: input validation

	err := c.svc.ProcessReport(body.ServerName, body.StartTime, body.EndTime)
	if err != nil {
		log.Println(err)
		sendError(err, w, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (c *controller) ProcessStatistics(w http.ResponseWriter, r *http.Request) {
	stats, err := c.svc.ProcessStatistics()
	if err != nil {
		log.Println(err)
		sendError(err, w, http.StatusInternalServerError)
		return
	}

	body, _ := json.Marshal(stats)
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (c *controller) ProcessOutliers(w http.ResponseWriter, r *http.Request) {
	result, err := c.svc.ProcessOutliers()
	if err != nil {
		log.Println(err)
		sendError(err, w, http.StatusInternalServerError)
		return
	}

	body, _ := json.Marshal(result)

	// TODO: return 404 on empty result?
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

// TODO: don't return golang errors to the customer
func sendError(err error, w http.ResponseWriter, status int) {
	body, _ := json.Marshal(map[string]string{"error": err.Error()})
	w.WriteHeader(status)
	w.Write(body)
}
